// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
    siteName: "Gridsome",
    ison: {
        favicon: "./src/favicon.png",
        touchicon: "./src/favicon.png",
    },
    plugins: [{
        use: "gridsome-plugin-pwa",
        options: {
            // Service Worker Options
            disableServiceWorker: false,
            serviceWorkerPath: "service-worker.js",
            cachedFileTypes: "js,json,css,html,png,jpg,jpeg,svg,gif",
            disableTemplatedUrls: false, // Optional

            // Manifest Options (https://developer.mozilla.org/en-US/docs/Web/Manifest)
            manifestPath: "manifest.json",
            title: "Gridsome",
            startUrl: "/",
            display: "standalone",
            statusBarStyle: "default",
            themeColor: "#666600",
            backgroundColor: "#ffffff",
            icon: "./src/favicon.png",

            /* shortName: 'Gridsome',              // Optional
                            description: 'Gridsome is awesome!',// Optional
                            categories: ['education'],          // Optional
                            lang: 'en-GB',                      // Optional
                            dir: 'auto',                        // Optional
                            maskableIcon: true,                 // Optional
                           screenshots: [                      // Optional
                                {
                                    src: 'src/screenshot1.png',
                                    sizes: '1280x720',
                                    type: 'image/png',
                                },
                            ],
                            shortcuts: [                        // Optional
                                {
                                    name: "View Subscriptions",
                                    short_name: "Subscriptions",
                                    description: "View the list of podcasts you listen to",
                                    url: "/subscriptions?utm_source=homescreen",
                                    icons: [{ src: "/icons/subscriptions.png", sizes: "192x192" }]
                                }
                            ],
                            gcmSenderId: undefined,  */ // Optional

            // Standard Meta Tags
            //svgFavicon: 'favicon.svg',          // Optional. Requires favicon.ico fallback

            // Microsoft Windows Meta Tags
            //msTileColor: '#666600',             // Optional

            // Apple MacOS Meta Tags
            //appleMaskIcon: 'favicon.svg',       // Optional
            //appleMaskIconColor: '#666600',      // Optional
        },
    }, ],
};